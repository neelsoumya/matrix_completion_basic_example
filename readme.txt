Very basic generic function to do basic matrix completion.
Has code adapted from
https://web.stanford.edu/~hastie/swData/softImpute/vignette.html

Install softImpute
in R
install.packages("softImpute")

Usage:
nohup R --no-save < call_function.R